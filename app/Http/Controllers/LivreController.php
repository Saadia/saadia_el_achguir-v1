<?php

namespace App\Http\Controllers;

use App\Models\Livre;
use Illuminate\Http\Request;

class LivreController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $livresBuilder=Livre::query();
        $livres=$livresBuilder->paginate(20);
        return view("livres.index",compact("livres"));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("livres.create");
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validateData=$request->validate([
            "titre"=>"required|max:255",
            "pages"=>"required|number",
            "description"=>"required",
            "categorie_id"=>"required",
        ]);
        if ($request->hasFile("image")) {
            $imagePath = $request->file("image")->store("livres/images", "public");
            $validateData["image"] = $imagePath;
        }
        Livre::create($validateData);
        return redirect()->route("livres.index");
    }

    /**
     * Display the specified resource.
     */
    public function show(Livre $livre)
    {
        return view("livres.show",compact("livre"));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Livre $livre)
    {
        return view("livres.edit",compact("livre"));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $validateData=$request->validate([
            "titre"=>"required|max:255",
            "pages"=>"required|number",
            "description"=>"required",
            "categorie_id"=>"required",
        ]);
        if ($request->hasFile("image")) {
            $imagePath = $request->file("image")->store("livres/images", "public");
            $validateData["image"] = $imagePath;
        }
        $livre = Livre::find($id);
        $livre->update($validateData);
        return redirect()->route("livres.index");
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Livre::destroy($id);
        return redirect()->route("livres.index");
    }
}
