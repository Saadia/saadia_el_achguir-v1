@extends('layouts.app')
@section('titre', 'Livres')
@section('content')
    <button class="btn btn-primary"><a href={{ route('livres.create') }}>Ajouter un livre</a> </button>
    <div class="table-responsive">
        <table class="table table-primary">
            <thead>
                <tr>
                    <th scope="col">Image</th>
                    <th scope="col">Titre</th>
                    <th scope="col">Categorie</th>
                    <th scope="col">Pages</th>
                    <th scope="col">Description</th>
                    <th scope="col" colspan="2">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($livres as $livre)
                    <tr class="">
                        <td scope="row"><img src="{{ asset('storage' . $livre->image) }}" alt=""> </td>
                        <td scope="row">{{ $livre->titre }}</td>
                        <td scope="row">{{ $livre->categorie->nom }}</td>
                        <td>{{ $livre->pages }}</td>
                        <td>{{ $livre->description }}</td>
                        <td>
                            <button><a href="{{ route('livres.edit', ['livre' => $livre->id]) }}">Modifier</a></button>
                        </td>
                        <td>
                            <form action="{{ route('livres.destroy', ['livre' => $livre->id]) }}" method="POST">
                                @csrf
                                @method('delete')
                                <input type="submit" value="Supprimer">
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
