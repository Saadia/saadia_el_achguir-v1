<?php

namespace Database\Factories;

use App\Models\Categorie;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\livre>
 */
class LivreFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            "titre"=>fake()->sentence(),
            "pages"=>fake()->randomFloat(2,0,500),
            "descriptions"=>fake()->text(100),
            "categorie_id"=>Categorie::all()->random()->id,
        ];
    }
}
